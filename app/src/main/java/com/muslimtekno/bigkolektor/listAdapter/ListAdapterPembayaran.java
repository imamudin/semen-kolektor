package com.muslimtekno.bigkolektor.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muslimtekno.bigkolektor.R;
import com.muslimtekno.bigkolektor.model.Pembayaran;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by imamudin on 21/05/17.
 */
public class ListAdapterPembayaran extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Pembayaran> pembayarans;

    public ListAdapterPembayaran(Activity activity, List<Pembayaran> pembayarans) {
        this.activity = activity;
        this.pembayarans = pembayarans;
    }

    @Override
    public int getCount() {
        return pembayarans.size();
    }

    @Override
    public Object getItem(int location) {
        return pembayarans.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_pembayaran, null);

        TextView nama           = (TextView) convertView.findViewById(R.id.tv_nama);
        TextView tgl            = (TextView) convertView.findViewById(R.id.tv_tgl_pembayaran);
        TextView jenis          = (TextView) convertView.findViewById(R.id.tv_jenis_pembayaran);
        TextView bukti          = (TextView) convertView.findViewById(R.id.tv_bukti);
        TextView tagihan        = (TextView) convertView.findViewById(R.id.tv_tagihan);
        TextView jumlah         = (TextView) convertView.findViewById(R.id.tv_jumlah_pembayaran);


        Pembayaran m = pembayarans.get(position);

        nama.setText(m.nama);
        tgl.setText(m.tgl);
        jenis.setText(m.jenis);
        bukti.setText(m.bukti);
        tagihan.setText(decimalToRupiah(Double.parseDouble(m.tagihan)));
        jumlah.setText(decimalToRupiah(Double.parseDouble(m.jumlah)));

        return convertView;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
}
