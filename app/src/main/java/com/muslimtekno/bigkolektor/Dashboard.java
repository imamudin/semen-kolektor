package com.muslimtekno.bigkolektor;

/**
 * Created by imamudin on 12/01/17.
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.muslimtekno.bigkolektor.app.MyAppController;
import com.muslimtekno.bigkolektor.config.GlobalConfig;
import com.muslimtekno.bigkolektor.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigkolektor.util.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Map;

public class Dashboard extends AppCompatActivity{
    LinearLayout ll_main;
    TextView tv_nama_user;
    LinearLayout  ll_piutang, ll_pembayaran, ll_logout, ll_history_pembayaran;
    boolean firt_time;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;

    //untuk filter
    LinearLayout ll_pilih_toko, ll_tgl_mulai, ll_tgl_selesai;
    Button btn_simpan, btn_kosongkan;
    TextView tv_nama_toko, tv_tgl_mulai, tv_tgl_selesai;
    Calendar c_awal, c_akhir;
    MyLog myLog;
    //tutup untuk filter

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        init();
        init_filter();
        setTanggalHariIni();
    }
    private void init(){
        firt_time = true;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Dashboard.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        if(!pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openLogin();
            Log.i("login", "buka main");
        }

        ll_piutang          = (LinearLayout)findViewById(R.id.ll_piutang);
        ll_pembayaran       = (LinearLayout)findViewById(R.id.ll_pembayaran);
        ll_logout           = (LinearLayout)findViewById(R.id.ll_logout);
        ll_history_pembayaran= (LinearLayout)findViewById(R.id.ll_history_pembayaran);

        tv_nama_user    = (TextView)findViewById(R.id.tv_nama_user);
        ll_piutang.setOnClickListener(btnClick);
        ll_pembayaran.setOnClickListener(btnClick);
        ll_logout.setOnClickListener(btnClick);
        ll_history_pembayaran.setOnClickListener(btnClick);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
        tv_nama_user.setText(pref.getString(GlobalConfig.USER_NAME, ""));
    }

    private void init_filter(){
        ll_pilih_toko   = (LinearLayout)findViewById(R.id.ll_pilih_toko);
        ll_tgl_mulai    = (LinearLayout)findViewById(R.id.ll_tgl_mulai);
        ll_tgl_selesai  = (LinearLayout)findViewById(R.id.ll_tgl_selesai);

        btn_kosongkan   = (Button) findViewById(R.id.btn_kosongkan);
        btn_simpan      = (Button) findViewById(R.id.btn_simpan);

        tv_nama_toko    = (TextView) findViewById(R.id.tv_nama_toko);
        tv_tgl_mulai    = (TextView) findViewById(R.id.tv_tgl_mulai);
        tv_tgl_selesai  = (TextView) findViewById(R.id.tv_tgl_selesai);

        tv_tgl_mulai.setText(pref.getString(GlobalConfig.FILTER_TGL_MULAI_S,"-"));
        tv_tgl_selesai.setText(pref.getString(GlobalConfig.FILTER_TGL_AKHIR_S,"-"));

        ll_pilih_toko.setOnClickListener(btnClickfilter);
        ll_tgl_mulai.setOnClickListener(btnClickfilter);
        ll_tgl_selesai.setOnClickListener(btnClickfilter);
        btn_kosongkan.setOnClickListener(btnClickfilter);
        btn_simpan.setOnClickListener(btnClickfilter);

        //inisialia awal dilai calendar jika pref sudah ada
        try {
            Log.i(GlobalConfig.TAG, "Set tanggal awal.");
            setTanggalHariIni();
        }catch (Exception e){
            Log.i(GlobalConfig.TAG, "tidak dapat memproses tanggal.");
        }

        if(!pref.getString(GlobalConfig.FILTER_TOKOID, "").equals("")){
            tv_nama_toko.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
        }
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==ll_piutang){
                open = new Intent(Dashboard.this, DaftarPiutang.class);
                startActivity(open);
//                String id_toko    = pref.getString(GlobalConfig.FILTER_TOKOID,"");
//                if(id_toko.equals("")){
//                    notifikasi("Silakan pilih toko.");
//                }else {
//                    open = new Intent(Dashboard.this, DaftarPiutang.class);
//                    startActivity(open);
//                }
                return;
            }else  if(v == ll_history_pembayaran){
                open = new Intent(Dashboard.this, HistoryPembayaran.class);
                startActivity(open);
            }else  if(v == ll_pembayaran){
                open = new Intent(Dashboard.this, PilihTokoPembayaran.class);
                startActivity(open);
            }else  if(v == ll_logout){
                dialogLogout();
            }
        }
    };
    View.OnClickListener btnClickfilter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                simpan();
                return;
            }else if(v==btn_kosongkan){
                Calendar c = Calendar.getInstance();
                tv_nama_toko.setText("");

                pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                setTanggalHariIni();

                Toast.makeText(Dashboard.this, "Filter telah dihapus.", Toast.LENGTH_SHORT).show();

//                setResult(RESULT_OK);
//                finish();
                return;
            }else if(v==ll_pilih_toko){
                Intent pilih_toko = new Intent(Dashboard.this, PilihToko.class);
                startActivityForResult(pilih_toko, GlobalConfig.KODE_INTENT_PILIH_TOKO);
                return;
            }else if(v==ll_tgl_mulai){
                dateTimePick(tv_tgl_mulai);
                return;
            }else if(v==ll_tgl_selesai){
                dateTimePick(tv_tgl_selesai);
                return;
            }
        }
    };
    private void  simpan(){
        if(c_awal == null || c_akhir==null || tv_tgl_mulai.getText().equals("-") || tv_tgl_selesai.getText().equals("-")){
            //notifikasi("Masukan tanggal mulai dan akhir.");
        }else {
            if (c_awal.getTimeInMillis() > c_akhir.getTimeInMillis()) {
                notifikasi("Tanggal mulai harus lebih rendah.");
            } else {View.OnClickListener btnClickfilter = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v==btn_simpan){
                        simpan();
                        return;
                    }else if(v==btn_kosongkan){
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        tv_tgl_mulai.setText(""+format_s.format(c.getTime()));
                        tv_tgl_selesai.setText(""+format_s.format(c.getTime()));
                        tv_nama_toko.setText("");

                        pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                        pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                        pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();

                        setTanggalHariIni();

                        Toast.makeText(Dashboard.this, "Filter telah dihapus.", Toast.LENGTH_SHORT).show();

//                setResult(RESULT_OK);
//                finish();
                        return;
                    }else if(v==ll_pilih_toko){
                        Intent pilih_toko = new Intent(Dashboard.this, PilihToko.class);
                        startActivityForResult(pilih_toko, GlobalConfig.KODE_INTENT_PILIH_TOKO);
                        return;
                    }else if(v==ll_tgl_mulai){
                        dateTimePick(tv_tgl_mulai);
                        return;
                    }else if(v==ll_tgl_selesai){
                        dateTimePick(tv_tgl_selesai);
                        return;
                    }
                }
            };

                //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), tv_tgl_mulai.getText()+" === "+tv_tgl_selesai.getText());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format.format(c_awal.getTime())+", akhir : "+format.format(c_akhir.getTime()));

                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
            }
        }
        Toast.makeText(Dashboard.this, "Filter telah disimpan.", Toast.LENGTH_SHORT).show();
        //setResult(RESULT_OK);
        //finish();
    }
    private void scanQRCode(){
        IntentIntegrator integrator = new IntentIntegrator(Dashboard.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Silakan arahkan ke barcode toko.");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
    }
    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                Log.i("Camera", "G : " + grantResults[0]);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.i(getLocalClassName(), "akses kamera diizinkan");
                    scanQRCode();
                    return;
                } else {
                    Log.i(getLocalClassName(), "akses kamera tidak diizinkan");

                    //It will return false if the user tapped “Never ask again”.
                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.CAMERA);
                    if (is_Neveraskagain) {
                        //showAlert();
                        Log.i(getLocalClassName(), "is_Neveraskagain false");
                    } else if (!is_Neveraskagain) {
                        Log.i(getLocalClassName(), "is_Neveraskagain true");
                        if(!pref.getBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, false)){
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : false");
                            pref.edit().putBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, true).commit();
                        }else{
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : true");
                            startInstalledAppDetailsActivity(Dashboard.this);
                        }
                    }
                    return;
                }
            }
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                Log.i("Camera", "G : " + grantResults[0]);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.i(getLocalClassName(), "akses lokasi diizinkan");
                    return;
                } else {
                    Log.i(getLocalClassName(), "akses lokasi tidak diizinkan");

                    //It will return false if the user tapped “Never ask again”.
                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA);
                    if (is_Neveraskagain) {
                        //showAlert();
                        Log.i(getLocalClassName(), "is_Neveraskagain false");
                    } else if (!is_Neveraskagain) {
                        Log.i(getLocalClassName(), "is_Neveraskagain true");
                        if(!pref.getBoolean(GlobalConfig.PERMISIION_LOCATION_FIRST_TIME, false)){
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : false");
                            pref.edit().putBoolean(GlobalConfig.PERMISIION_LOCATION_FIRST_TIME, true).commit();
                        }else{
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : true");
                            startInstalledAppDetailsActivity(Dashboard.this);
                        }
                    }
                    return;
                }
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GlobalConfig.KODE_INTENT_PILIH_TOKO:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        String nama_toko    = pref.getString(GlobalConfig.FILTER_TOKO_NAMA,"Pilih toko");
                        if(nama_toko.equals("")){
                            nama_toko = "Pilih toko";
                        }
                        tv_nama_toko.setText(nama_toko);
                        simpan();
                        break;
                }
                break;
        }
    }
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(2, 2, 2, "Logout").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void dateTimePick(final TextView editText){
        final View dialogView = View.inflate(this, R.layout.date_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
        dp_awal.setMaxDate(System.currentTimeMillis());
        if(editText==tv_tgl_mulai){
            dp_awal.updateDate(c_awal.get(Calendar.YEAR),c_awal.get(Calendar.MONTH),c_awal.get(Calendar.DATE));
        }else {
            dp_awal.updateDate(c_akhir.get(Calendar.YEAR),c_akhir.get(Calendar.MONTH),c_akhir.get(Calendar.DATE));
            if (!tv_tgl_mulai.getText().toString().trim().equals("")) {
                String mytime = tv_tgl_mulai.getText().toString().trim();
                Log.i(getLocalClassName(), mytime);
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd MMM yyyy");
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(mytime);
                    dp_awal.setMinDate(myDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.i(getLocalClassName(), "error");
                }
            }else{
                Log.i(getLocalClassName(), "");
                dp_awal.updateDate(c_akhir.get(Calendar.YEAR),c_akhir.get(Calendar.MONTH),c_akhir.get(Calendar.DATE));
            }
        }

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                        dp_awal.getMonth(),
                        dp_awal.getDayOfMonth());

                if(editText==tv_tgl_mulai){
                    c_awal = cal;
                }else{
                    c_akhir = cal;
                }

                SimpleDateFormat format_show = new SimpleDateFormat("dd MMM yyyy");
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format_show.format(cal.getTime()));
                editText.setText(format_show.format(cal.getTime()));

                //menyimpan perubahan tanggal dan nama toko
                simpan();

                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void dialogLogout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Apakah yakin anda akan keluar?")
                .setCancelable(false)
                .setNegativeButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        logout();
                    }
                })
                .setPositiveButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
        ;
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void logout(){
        Log.i(GlobalConfig.TAG, "make progress dialog2");
        loading = ProgressDialog.show(Dashboard.this, "Logout...", "Mohon tunggu...", true);

        String url = "";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }else{
            url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.i(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            pref.edit().remove(GlobalConfig.USER_ID).commit();
                            pref.edit().remove(GlobalConfig.USER_TOKEN).commit();
                            pref.edit().remove(GlobalConfig.IS_LOGIN).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                            pref.edit().remove(GlobalConfig.FILTER_TGL_MULAI).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TGL_AKHIR).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TGL_MULAI_S).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TGL_AKHIR_S).commit();

                            openLogin();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.i("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //Log.i("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void openLogin(){
        Intent login = new Intent(Dashboard.this, Login.class);
        startActivity(login);
        Dashboard.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    private void setTanggalHariIni(){
        Log.i(GlobalConfig.TAG, "Set tanggal awal.");
        c_awal = Calendar.getInstance();
        c_awal.set(Calendar.DATE, 1);

        c_akhir = Calendar.getInstance();

        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        tv_tgl_mulai.setText(""+format_s.format(c_awal.getTime()));
        tv_tgl_selesai.setText(""+format_s.format(c_akhir.getTime()));

        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
    }
}