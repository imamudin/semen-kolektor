package com.muslimtekno.bigkolektor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.muslimtekno.bigkolektor.config.GlobalConfig;
import com.muslimtekno.bigkolektor.model.Pembayaran;
import com.muslimtekno.bigkolektor.mysp.ObscuredSharedPreferences;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by imamudin on 22/05/17.
 */
public class DetailTagihan extends AppCompatActivity {
    LinearLayout ll_main;
    ObscuredSharedPreferences pref;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_tagihan);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Pembayaran");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(DetailTagihan.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        TextView nama_toko      = (TextView) findViewById(R.id.tv_nama_toko);
        TextView nomor_faktur   = (TextView) findViewById(R.id.tv_no_faktur);
        TextView tgl_faktur     = (TextView) findViewById(R.id.tv_tgl_faktur);
        TextView tagihan        = (TextView) findViewById(R.id.tv_tagihan);
        TextView bayar          = (TextView) findViewById(R.id.tv_bayar);
        TextView tgl_bayar      = (TextView) findViewById(R.id.tv_tgl_bayar);
        TextView tgl_notif      = (TextView) findViewById(R.id.tv_tgl_notif);
        TextView tgl_jatuh_tempo= (TextView) findViewById(R.id.tv_tgl_jatuh_tempo);


        Intent old  = getIntent();
        String datString = old.getStringExtra("data");
        Pembayaran m = new Pembayaran(datString);

//        nama_toko.setText(m.NAMA);
//        nomor_faktur.setText("Faktur : "+m.NO_FAKTUR);
//        tgl_faktur.setText(m.TGL_FAKTUR);
//        tagihan.setText(decimalToRupiah(Double.parseDouble(m.PIUTANG)));
//        bayar.setText(decimalToRupiah(Double.parseDouble(m.PEMBAYARAN)));
//        tgl_bayar.setText(m.TGL_PEMBAYARAN);
//        tgl_notif.setText(m.TGL_NOTIF);
//        tgl_jatuh_tempo.setText(m.TGL_JATUH_TEMPO);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
    }

    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
