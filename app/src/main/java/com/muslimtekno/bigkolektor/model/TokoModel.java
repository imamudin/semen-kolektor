package com.muslimtekno.bigkolektor.model;

/**
 * Created by imamudin on 22/05/17.
 */
public class TokoModel {
    public String KODE, NAMA, ALAMAT;

    public TokoModel(String KODE, String NAMA, String ALAMAT) {
        this.KODE = KODE;
        this.NAMA = NAMA;
        this.ALAMAT = ALAMAT;
    }
}
