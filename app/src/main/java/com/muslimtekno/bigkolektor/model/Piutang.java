package com.muslimtekno.bigkolektor.model;

import android.util.Log;

import com.muslimtekno.bigkolektor.config.GlobalConfig;

import org.json.JSONObject;

/**
 * Created by imamudin on 28/03/17.
 */
public class Piutang {
    public String KODE, NAMA, ALAMAT, JUMLAH_FAKTUR, TGL_JATUH_TEMPO;
    public Double PIUTANG;
    String dataJson;

    public Piutang(String object){
        this.dataJson   = object;
        try{
            JSONObject o = new JSONObject(object);

            this.KODE        = o.getString(GlobalConfig.GP_KODE);
            this.NAMA        = o.getString(GlobalConfig.GP_NAMA_TOKO);
            this.ALAMAT      = o.getString(GlobalConfig.GP_ALAMAT);
            this.PIUTANG     = Double.parseDouble(o.getString(GlobalConfig.GP_PIUTANG));
            this.JUMLAH_FAKTUR= o.getString(GlobalConfig.GP_JUMLAH_FAKTUR);
            this.TGL_JATUH_TEMPO= convertNull(o.getString(GlobalConfig.GP_TGL_JATUH_TEMPO));
        }catch (Exception e){
            Log.d(getClass().toString(),"error memproses data object.");
        }
    }

    private String convertNull(String old){
        if(old == "null" || old.equals("null"))
            old = "-";
        return old;
    }
}
