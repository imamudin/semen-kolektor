package com.muslimtekno.bigkolektor.model;

import android.util.Log;

import com.muslimtekno.bigkolektor.config.GlobalConfig;

import org.json.JSONObject;

/**
 * Created by imamudin on 21/05/17.
 */
public class Pembayaran {
    public String nama, tgl, jenis, bukti, tagihan, jumlah;
    public String dataJson;

    public Pembayaran(){}
    public Pembayaran(String object){
        this.dataJson   = object;
        try{
            JSONObject o = new JSONObject(object);

            this.nama       = o.getString(GlobalConfig.G_PEMBAYARAN_NAMA);
            this.tgl        = o.getString(GlobalConfig.G_PEMBAYARAN_TGL);
            this.jenis      = o.getString(GlobalConfig.G_PEMBAYARAN_JENIS);
            this.bukti      = o.getString(GlobalConfig.G_PEMBAYARAN_BUKTI);
            this.tagihan    = convertNull(o.getString(GlobalConfig.G_PEMBAYARAN_TAGIHAN));
            this.jumlah     = o.getString(GlobalConfig.G_PEMBAYARAN_JUMLAH);
        }catch (Exception e){
            Log.d(getClass().toString(),"error memproses data object.");
        }
    }
    private String convertNull(String old){
        if(old == "null" || old.equals("null"))
            old = "-";
        return old;
    }
}
