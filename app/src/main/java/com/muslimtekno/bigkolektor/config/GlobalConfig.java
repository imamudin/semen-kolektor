package com.muslimtekno.bigkolektor.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by agung on 09/03/2016.
 */
public class GlobalConfig {
    public static final String ENVIRONMENT      = "development";   //production  -> log tidak muncul, development -> log muncul
    public static final String APP_ID           = "Kdia83nds9u3msdkJds";       //key untuk menerima gcm
    public static final String APP_TOKEN        = "appToken";                //key untuk mengirim app
    public static final String NAMA_PREF        = "com.imamudin.semen.pref";  //key untuk menyimpan file preferences

    //public static final String IP               = "http://103.76.171.101";          //alamat web/ip server
    public static final String IP               = "http://192.168.8.100";          //alamat web/ip local
    public static final String IP_KEY           = "ip";                     //key untuk alamat web/ip website
    public static final String WEB_URL          = "/apibig";                           //nama domain url

    public static final String TAG              = "mybig";                  //key untuk LOG
    public static final int MY_SOCKET_TIMEOUT_MS    = 30000;                    //30 detik
    public static final int MAX_ROW_PER_REQUEST     = 25;                       //jumlah baris tiap request
    public static final int KODE_INTENT_PILIH_TOKO          = 102;

    //permisiion untuk marshmallow pertama kali
    public static final String PERMISIION_CAMERA_FIRST_TIME     = "PERMISIION_CAMERA_FIRST_TIME";
    public static final String PERMISIION_LOCATION_FIRST_TIME   = "PERMISIION_LOCATION_FIRST_TIME";

    //URL
    public static final String URL_LOGIN            = "/login_kolektor";
    public static final String URL_LOGOUT           = "/logout_kolektor";

    public static final String URL_PIUTANGS         = "/piutang_kolektor";
    public static final String URL_DETAIL_PIUTANG   = "/detail_piutang_koletor";
    public static final String URL_HISTORY_PEMBAYARAN = "/history_pembayaran_kolektor";
    public static final String URL_TOKOS            = "/toko_kolektor";
    public static final String URL_PEMBAYARAN_TAGIHAN= "/pembayaran_tagihan";

    //PEMBAYARAN FAKTUR
    public static final String URL_BAYAR_FAKTUR     = "/pembayaranfaktur";
    public static final String URL_SCANTOKO         = "/scantokopembayaran";


    //NOTIFIKASI
    public static final String notif_form_tidak_kosong  = "Username atau password tidak boleh kosong!";
    public static final String notif_form_tidak_cocok   = "Username atau password tidak cocok!";
    public static final String notif_butuh_koneksi      = "Aplikasi membutuhkan koneksi internet!";

    public static final String USER_NAME    = "user_name";
    public static final String USER_PASWORD = "user_password";
    public static final String USER_REGID   = "user_regid";
    public static final String IS_LOGIN     = "is_login";
    public static final String USER_ID      = "id_user";
    public static final String USER_TOKEN   = "user_token";

    //untuk pembayaran model
    public static final String G_PEMBAYARAN_NAMA    = "nama";
    public static final String G_PEMBAYARAN_TGL     = "tgl_pembayaran";
    public static final String G_PEMBAYARAN_JENIS   = "jenis_pembayaran";
    public static final String G_PEMBAYARAN_BUKTI   = "bukti";
    public static final String G_PEMBAYARAN_TAGIHAN = "tagihan";
    public static final String G_PEMBAYARAN_JUMLAH  = "jumlah_pembayaran";

    public static final String UP_START             = "START";
    public static final String UP_LIMIT             = "LIMIT";
    public static final String UP_KODE              = "KODE";
    public static final String UP_TGL_AWAL          = "TGL_AWAL";
    public static final String UP_TGL_AKHIR         = "TGL_AKHIR";

    //untuk bayar faktur
    public static final String BAYAR_JENIS_BAYAR    = "JENIS_PEMBAYARAN";
    public static final String BAYAR_MODEL_BAYAR    = "MODEL_PEMBAYARAN";
    public static final String BAYAR_INVOICE        = "INVOICE";
    public static final String BAYAR_INVOICE_AKSI   = "INVOICE_AKSI";
    public static final String BAYAR_INVOICE_BAYAR  = "INVOICE_BAYAR";
    public static final String BAYAR_FK_TOTAL       = "TOTAL";
    public static final String BAYAR_FK_TELEPON     = "TELEPON";
    public static final String BAYAR_IS_BAYAR       = "IS_BAYAR";
    public static final String BAYAR_ID_KET         = "ID_KET";


    public static final String GP_KODE              = "KODE";
    public static final String GP_NAMA_TOKO         = "NAMA";
    public static final String GP_ALAMAT            = "ALAMAT";
    public static final String GP_PIUTANG           = "PIUTANG";
    public static final String GP_JUMLAH_FAKTUR     = "JUMLAH_FAKTUR";
    public static final String GP_TGL_JATUH_TEMPO   = "TGL_JATUH_TEMPO";


    public static final String GDP_NO_FAKTUR        = "NO_FAKTUR";
    public static final String GDP_TGL_FAKTUR       = "TGL_FAKTUR";
    public static final String GDP_TOTAL            = "TOTAL";
    public static final String GDP_JATUH_TEMPO      = "TGL_JATUH_TEMPO";
    public static final String GDP_BELUM_DIBAYAR    = "PIUTANG";

    public static final String GT_KODE              = "KODE";
    public static final String GT_NAMA              = "NAMA";
    public static final String GT_ALAMAT            = "ALAMAT";

    public static final String FILTER_TOKOID        = "KODE";
    public static final String FILTER_TOKO_NAMA     = "NAMA";
    public static final String FILTER_TOKO_ALAMAT   = "ALAMAT";
    public static final String FILTER_TGL_MULAI     = "TGL_AWAL";
    public static final String FILTER_TGL_MULAI_S   = "TGL_AWAL_SHOW";
    public static final String FILTER_TGL_AKHIR     = "TGL_AKHIR";
    public static final String FILTER_TGL_AKHIR_S   = "TGL_AKHIR_SHOW";

    //VARIABEL INVOICE
    public static final String GINVOICE_BUKTI       = "bukti";
    public static final String GINVOICE_TGL_ORDER   = "tgl_order";
    public static final String GINVOICE_TGL_JTEMPO  = "tgl_jtempo";
    public static final String GINVOICE_TAGIHAN     = "tagihan";
    public static final String GINVOICE_TERBAYAR    = "terbayar";
    public static final String GINVOICE_KEKURANGAN  = "kekurangan";

    //VARIABEL KETERANGNA
    public static final String GINVOICE_ID_KET      = "ID_KET";
    public static final String GINVOICE_KETERANGAN  = "KETERANGAN";

    //VARIABEL DETAIL TOKO
    public static final String IS_TOKO          = "IS_TOKO";
    public static final String ID_TOKO          = "KODE";
    public static final String GTOKO_NAMA       = "NAMA";
    public static final String GTOKO_ALAMAT     = "ALAMAT";
    public static final String GTOKO_PEMILIK    = "PIMPINAN";
    public static final String GREAL_PLAFON     = "REALPLAFON";
    public static final String GKDSALES         = "KDSALES";
    public static final String GJATUHTEMPOBAYAR = "JATUHTEMPOBAYAR";
    public static final String GTOKO_NOTELP     = "TELEPON";
    public static final String GTOKO_LAT        = "LATITUDE";
    public static final String GTOKO_LONG       = "LONGITUDE";
    public static final String GTOKO_NIK        = "NIK";
    public static final String GTOKO_POIN       = "POIN";
    public static final String GTOKO_PIUTANG_KAP        = "PIUTANG";
    public static final String GTOKO_TOTAL_PIUTANG_KAP  = "TOTAL_PIUTANG";
    public static final String GTOKO_REALPLAFON         = "REALPLAFON";
    public static final String GTOKO_TAGIHAN_TERDEKAT   = "TAGIHAN_TERDEKAT";
    public static final String GTOKO_TGL_JATUH_TEMPO    = "TGL_JATUH_TEMPO";
    public static final String GTOKO_TOTAL_PIUTANG      = "total_piutang";

    public static final String GTOKO_INVOICE            = "invoice";
    public static final String GTOKO_KETERANGAN         = "keterangan";


    //untuk status pemesanan
    public static final Map<Integer, String> bulanToString;
    static
    {
        bulanToString = new HashMap<Integer, String>();
        bulanToString.put(1, "Januari");
        bulanToString.put(2, "Februari");
        bulanToString.put(3, "Maret");
        bulanToString.put(4, "April");
        bulanToString.put(5, "Mei");
        bulanToString.put(6, "Juni");
        bulanToString.put(7, "Juli");
        bulanToString.put(8, "Agustus");
        bulanToString.put(9, "September");
        bulanToString.put(10, "Oktober");
        bulanToString.put(11, "November");
        bulanToString.put(12, "Desember");

    };

    public static final Map<Integer, String> invoiceBayar;
    static
    {
        invoiceBayar = new HashMap<Integer, String>();
        invoiceBayar.put(0, "Belum Bayar");
        invoiceBayar.put(1, "Dilunasi");
        invoiceBayar.put(2, "Dicicil");
    };


//    [11:09, 11/5/2018] Mas Pii: Gung
//[11:11, 11/5/2018] Mas Pii: Dikon gawe inputan
//[11:14, 11/5/2018] Mas Pii: dadi nang dashboar iku ditambahi tombol pembayaran
//[11:14, 11/5/2018] Mas Pii: Model e koyo set lokasi ne sales
//[11:14, 11/5/2018] Mas Pii: Dadi pas di klik tombol pembayaran, metu tampilan pilih toko.
//[11:17, 11/5/2018] Mas Pii: Bari toko ne dipilih terus metu halaman input pembayaran. Isi inputane : no faktur, jumlah pembayaran.
}
