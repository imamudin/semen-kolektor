package com.muslimtekno.bigkolektor;

/**
 * Created by agung on 19/02/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.muslimtekno.bigkolektor.app.MyAppController;
import com.muslimtekno.bigkolektor.config.GlobalConfig;
import com.muslimtekno.bigkolektor.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigkolektor.util.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

//untuk google cloud messaging

public class Login extends AppCompatActivity {
    Button btn_login;
    EditText et_user_name, et_password;
    String s_user_name, s_password;
    RelativeLayout ll_main;

    ObscuredSharedPreferences pref;
    private static final int ACTION_PLAY_SERVICES_DIALOG = 100;
    ProgressDialog loading;
    MyLog myLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);

        init();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cek apakah hp konek internet
                ConnectivityManager cm =
                        (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    //cek email dan passwod tidak boleh kosong
                    s_user_name     = et_user_name.getText().toString().trim();
                    s_password      = et_password.getText().toString().trim();

                    if(s_user_name.length() != 0 && s_password.length() != 0){
                        login();
                    }else{
                        notifikasi(GlobalConfig.notif_form_tidak_kosong);
                    }
                }else{
                    notifikasi(GlobalConfig.notif_butuh_koneksi);
                }
            }
        });
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
        displayMessage(message);
    }
    @Override
    protected void onDestroy() {
        try {
            if (loading != null && loading.isShowing()) {
                loading.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
    private void init(){
        btn_login       = (Button)findViewById(R.id.btn_login);
        et_user_name    = (EditText)findViewById(R.id.et_user_name);
        et_password     = (EditText)findViewById(R.id.et_password);

        ll_main         = (RelativeLayout)findViewById(R.id.ll_main);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        if(pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openMainActivity();
            Log.d("login", "buka main");
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ip:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void login(){
        String token = FirebaseInstanceId.getInstance().getToken();
        loading = ProgressDialog.show(Login.this, "Login...", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGIN;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),token+" -- "+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_NAME, s_user_name);
            jsonBody.put(GlobalConfig.USER_PASWORD, s_password);
            jsonBody.put(GlobalConfig.USER_REGID, token);

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject data     = response.getJSONObject("data");

                            pref.edit().putBoolean(GlobalConfig.IS_LOGIN, true).commit();
                            pref.edit().putString(GlobalConfig.USER_ID, data.getString(GlobalConfig.USER_ID)).commit();
                            pref.edit().putString(GlobalConfig.USER_TOKEN, data.getString(GlobalConfig.USER_TOKEN)).commit();

                            pref.edit().putString(GlobalConfig.USER_NAME, data.getString(GlobalConfig.USER_NAME)).commit();

                            openMainActivity();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Volley errorl log"+error.toString());
                    NetworkResponse response = error.networkResponse;
                    //displayMessage("status Code : "+response.statusCode);
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }else{
                        displayMessage("Tidak dapat mengakses server.");
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                Map<String,String> headers = new Hashtable<String, String>();

                //Adding parameters
                headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }};
            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    //Somewhere that has access to a context
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    private void openMainActivity(){
        Intent dashboard = new Intent(Login.this, Dashboard.class);
        startActivity(dashboard);
        Login.this.finish();
    }
}