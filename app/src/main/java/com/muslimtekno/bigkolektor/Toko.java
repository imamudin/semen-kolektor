package com.muslimtekno.bigkolektor;

/**
 * Created by imamudin on 12/01/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigkolektor.app.MyAppController;
import com.muslimtekno.bigkolektor.config.GlobalConfig;
import com.muslimtekno.bigkolektor.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigkolektor.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class Toko extends AppCompatActivity {
    LinearLayout ll_main, ll_back_invoice, ll_list_invoice, ll_keterangan, ll_total_setor;
    Button btn_bayar;
    TextView tv_nama_toko, tv_poin, tv_alamat, tv_piutang, tv_jatuh_tempo, tv_realplafon, tv_tagihan_terdekat;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    AlertDialog alert_detail_piutang;
    MyLog myLog;
    EditText et_nohp, et_total;
    TextView tv_total;
    RadioGroup radiogroupMP, radiogroupJP;
    JSONArray json_invoice, json_aksi, json_bayar;
    RadioGroup radioIsBayar, radioKeterangan;

    Map<Integer, String> mKeterangan = new HashMap<Integer, String>();

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toko);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Pelanggan");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Toko.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        btn_bayar       = (Button)findViewById(R.id.btn_bayar);

        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_piutang      = (TextView)findViewById(R.id.tv_piutang);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_jatuh_tempo  = (TextView)findViewById(R.id.tv_jatuh_tempo);
        tv_realplafon   = (TextView)findViewById(R.id.tv_jumlah_plafon);
        tv_tagihan_terdekat  = (TextView)findViewById(R.id.tv_tagihan_terdekat);

        et_nohp         = (EditText) findViewById(R.id.et_nohp);
        et_total        = (EditText) findViewById(R.id.et_total);

        tv_total        = (TextView) findViewById(R.id.tv_total);

        radiogroupJP    = (RadioGroup) findViewById(R.id.radioJP);
        radiogroupMP    = (RadioGroup) findViewById(R.id.radioMP);
        ll_back_invoice = (LinearLayout) findViewById(R.id.ll_back_invoice);
        ll_list_invoice = (LinearLayout) findViewById(R.id.ll_list_invoice);
        ll_total_setor  = (LinearLayout) findViewById(R.id.ll_total_setor);

        json_invoice    = new JSONArray();
        json_aksi       = new JSONArray();
        json_bayar      = new JSONArray();

        radioIsBayar    = (RadioGroup) findViewById(R.id.radioBayar);
        radioKeterangan = (RadioGroup) findViewById(R.id.radioKeterangan);
        ll_keterangan   = (LinearLayout) findViewById(R.id.ll_keterangan);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));
        tv_poin.setText(pref.getString(GlobalConfig.GTOKO_POIN,""));
        tv_piutang.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP,"0"))));
        tv_jatuh_tempo.setText(pref.getString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO,""));
        tv_realplafon.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_REALPLAFON,"0"))));
        tv_tagihan_terdekat.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_TAGIHAN_TERDEKAT,"0"))));

        String no_hp = pref.getString(GlobalConfig.GTOKO_NOTELP,"");
        if(no_hp == "null" || no_hp.equals("null"))
            no_hp = "";
        et_nohp.setText(no_hp);

        btn_bayar.setOnClickListener(btnClick);

        ll_main             = (LinearLayout)findViewById(R.id.ll_main);

        et_total.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String s_total = et_total.getText().toString();
                if(s_total.length() <= 0){
                    s_total = "0";
                }
                tv_total.setText(decimalToRupiah(Double.parseDouble(s_total)));
            }
        });
        radiogroupJP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int radioButtonID = group.getCheckedRadioButtonId();
                View radioButton = group.findViewById(radioButtonID);
                int idx = group.indexOfChild(radioButton);

                et_total.setText("");
                json_invoice= new JSONArray();
                json_aksi   = new JSONArray();
                json_bayar  = new JSONArray();

                if(idx==0){ //untuk invoice
                    showInvoice(pref.getString(GlobalConfig.GTOKO_INVOICE, ""));
                    ll_total_setor.setVisibility(View.GONE);
                }else { //untuk setor
                    ll_total_setor.setVisibility(View.VISIBLE);
                    ll_back_invoice.setVisibility(View.GONE);
                }
            }
        });
        radioIsBayar.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int radioButtonID = group.getCheckedRadioButtonId();
                View radioButton = group.findViewById(radioButtonID);
                int idx = group.indexOfChild(radioButton);

                if(idx==0){ //untuk Ya
                    ll_keterangan.setVisibility(View.GONE);
                }else { //untuk Tidak
                    ll_keterangan.setVisibility(View.VISIBLE);
                }
            }
        });
        setKeterangan();
    }
    private void setKeterangan(){
        String dataKeterangan = pref.getString(GlobalConfig.GTOKO_KETERANGAN, "");

        try {
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), dataKeterangan);
            JSONArray datas = new JSONArray(dataKeterangan);
            for(int i=0;i<datas.length();i++){
                JSONObject keterangan  = datas.getJSONObject(i);
                String ID           = keterangan.getString(GlobalConfig.GINVOICE_ID_KET);
                String KETERANGAN   = keterangan.getString(GlobalConfig.GINVOICE_KETERANGAN);

                RadioButton radioButton = new RadioButton(this);
                radioButton.setText(KETERANGAN);
                radioButton.setId(i);
                radioButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

                if(i==0){
                    radioButton.setChecked(true);
                }

                mKeterangan.put(i, ID);
                radioKeterangan.addView(radioButton);
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), KETERANGAN);
            }
        }catch (Exception e){
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), e.getMessage().toString());
            Log.d(GlobalConfig.TAG, "Tidak dapat memproses data keterangan json.");
        }
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==btn_bayar){
                bayarFaktur();
                return;
            }

            if(open != null){
                startActivity(open);
            }else{
                notifikasi("Mohon maaf terjadi kesalahan.");
            }
        }
    };
    private void showInvoice(String dataString){
        ll_back_invoice.setVisibility(View.VISIBLE);
        ll_list_invoice.removeAllViews();
        try {
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), dataString);
            JSONArray datas = new JSONArray(dataString);
            for(int i=0;i<datas.length();i++){
                JSONObject piutang  = datas.getJSONObject(i);
                String bukti        = piutang.getString(GlobalConfig.GINVOICE_BUKTI);
                String tgl_order    = piutang.getString(GlobalConfig.GINVOICE_TGL_ORDER);
                String tgl_jtempo   = piutang.getString(GlobalConfig.GINVOICE_TGL_JTEMPO);
                String tagihan      = piutang.getString(GlobalConfig.GINVOICE_TAGIHAN);
                String terbayar     = piutang.getString(GlobalConfig.GINVOICE_TERBAYAR);
                String kekurangan   = piutang.getString(GlobalConfig.GINVOICE_KEKURANGAN);

                if(tgl_order == "null" || tgl_order.equals("null"))
                    tgl_order = "-";
                if(tgl_jtempo == "null" || tgl_jtempo.equals("null"))
                    tgl_jtempo = "-";

                ll_list_invoice.addView(getViewInvoice(bukti, tgl_order, tgl_jtempo,  tagihan, terbayar, kekurangan));
            }
        }catch (Exception e){
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), e.getMessage().toString());
            Log.d(GlobalConfig.TAG, "Tidak dapat memproses data json.");
        }
    }
    private View getViewInvoice(final String bukti, String tgl_order, String tgl_jtempo,
                                final String tagihan, String terbayar, final String kekurangan){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View data_history_view    = inflater.inflate(R.layout.list_invoice_add, null);

        final TextView tv_bukti     =(TextView)data_history_view.findViewById(R.id.tv_bukti);
        final TextView tv_tagihan_decimal   =(TextView)data_history_view.findViewById(R.id.tv_tagihan_decimal);
        final TextView tv_tgl_order =(TextView)data_history_view.findViewById(R.id.tv_tgl_order);
        final TextView tv_tagihan   =(TextView)data_history_view.findViewById(R.id.tv_tagihan);
        final TextView tv_tgl_jtempo=(TextView)data_history_view.findViewById(R.id.tv_jatuh_tempo);
        final TextView tv_sudah_dibayar =(TextView)data_history_view.findViewById(R.id.tv_sudah_dibayar);
        final TextView tv_kekurangan    =(TextView)data_history_view.findViewById(R.id.tv_kekurangan);
        final EditText et_total_invoice =(EditText) data_history_view.findViewById(R.id.et_total_invoice);
        final TextView tv_total_invoice = (TextView)  data_history_view.findViewById(R.id.tv_total_invoice);

        final Spinner sp_aksi       = (Spinner)data_history_view.findViewById(R.id.sp_aksi);

        tv_bukti.setText(bukti);
        tv_tgl_order.setText(tgl_order);
        tv_tagihan.setText(decimalToRupiah(Double.parseDouble(tagihan)));
        tv_tagihan_decimal.setText(""+tagihan);
        tv_tgl_jtempo.setText(tgl_jtempo);
        tv_sudah_dibayar.setText(decimalToRupiah(Double.parseDouble(terbayar)));
        tv_kekurangan.setText(decimalToRupiah(Double.parseDouble(kekurangan)));

        String[] spinnerArray = new String[GlobalConfig.invoiceBayar.size()];
        for (int i = 0; i < GlobalConfig.invoiceBayar.size(); i++)
        {
            spinnerArray[i] = GlobalConfig.invoiceBayar.get(i);
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        sp_aksi.setAdapter(spinnerArrayAdapter);
        sp_aksi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position==0){ //belum bayar
                    et_total_invoice.setEnabled(false);
                    et_total_invoice.setText("");
                }else if (position==1){ //dilunasi
                    et_total_invoice.setEnabled(true);
                    if(kekurangan != "" || !kekurangan.equals("")){
                        et_total_invoice.setText(kekurangan);
                    }else
                        et_total_invoice.setText(tagihan);
                }else {
                    et_total_invoice.setEnabled(true);
                    et_total_invoice.setText("");
                }
            }
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        et_total_invoice.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String s_total = et_total_invoice.getText().toString();
                if(s_total.length() <= 0){
                    s_total = "0";
                }
                tv_total_invoice.setText(decimalToRupiah(Double.parseDouble(s_total)));
            }
        });

        return data_history_view;
    }
    private void updateInvoice(){
        int id_json =0;
        json_invoice= new JSONArray();
        json_aksi   = new JSONArray();
        json_bayar  = new JSONArray();

        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(0);

        for (int i = 0; i < ll_list_invoice.getChildCount(); i++) {
            LinearLayout ll     = (LinearLayout) ll_list_invoice.getChildAt(i);
            LinearLayout ll0    = (LinearLayout) ll.getChildAt(0);
            LinearLayout ll00   = (LinearLayout) ll0.getChildAt(0);
            LinearLayout ll000  = (LinearLayout) ll00.getChildAt(0);

            LinearLayout ll008  = (LinearLayout) ll000.getChildAt(8);    //linear u/ jumlah
            Spinner sp_aksi     = (Spinner) ll008.getChildAt(1);    //linear u/ spinner

            int sp_id = sp_aksi.getSelectedItemPosition();

            if(sp_id != 0) {
                final TextView tv_bukti   = (TextView) ll000.getChildAt(0);
                LinearLayout ll009  = (LinearLayout) ll000.getChildAt(9);    //linear u/ jumlah
                EditText et_jumlah  = (EditText) ll009.getChildAt(1);    //linear u/ spinner

                String s_jumlah = et_jumlah.getText().toString();
                if(s_jumlah.equals("") || s_jumlah == "")
                    s_jumlah= "0";

                try {
                    json_invoice.put(id_json, tv_bukti.getText());
                    json_aksi.put(id_json, sp_id);
                    json_bayar.put(id_json, s_jumlah);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                id_json++;
            }
        }
    }
    private int getIsBayar(){
        if(getFromRadioGroup(radioIsBayar)==2){ //tidak bayar
            return 0;
        }else {
            return 1;
        }
    }
    private String getIDKet(){
        if(getFromRadioGroup(radioIsBayar)==2){ //tidak bayar
            return mKeterangan.get((getFromRadioGroup(radioKeterangan)-1));
        }else {
            return "";
        }
    }
    private void bayarFaktur(){
        boolean status = true;
        String message = "";
        String s_total = "";

        String s_nohp       = et_nohp.getText().toString();
        if(s_nohp.equals("")){
            status  = false;
            message = "Mohon untuk menambahkan Nomor HP.";
        }

        if(getFromRadioGroup(radiogroupJP) == 2){   //setor
            s_total      = et_total.getText().toString();
            if(s_total.length() <= 0){
                s_total = "0";
            }
            if(s_total.equals("0")){
                status  = false;
                message = "Mohon untuk menambahkan Total Pembayaran.";
            }
        }else { //invoice
            updateInvoice();
            if(json_invoice.length()<=0){
                status  = false;
                message = "Mohon untuk menambahkan Invoice.";
            }
            myLog.print("aksi : "+json_aksi.toString());
            myLog.print(json_invoice.toString());
            myLog.print(json_bayar.toString());

            for (int i =0; i<json_bayar.length(); i++){
                try {
                    String bayar = (String) json_bayar.get(i);
                    if(Double.parseDouble(bayar)<=0){
                        status  = false;
                        message = "Mohon untuk menambahkan nominal Invoice.";
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if(!status){
            notifikasi(message);
        }else {
            //kirim pembayaran
            String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_BAYAR_FAKTUR;
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);

            loading = ProgressDialog.show(Toko.this, "", "Mohon tunggu...", true);
            //bisa order
            JSONObject jsonBody;
            try {
                jsonBody = new JSONObject();
                jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
                jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

                jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.ID_TOKO, ""));
                jsonBody.put(GlobalConfig.BAYAR_JENIS_BAYAR, getFromRadioGroup(radiogroupJP));
                jsonBody.put(GlobalConfig.BAYAR_MODEL_BAYAR, getFromRadioGroup(radiogroupMP));
                jsonBody.put(GlobalConfig.BAYAR_INVOICE, json_invoice);
                jsonBody.put(GlobalConfig.BAYAR_INVOICE_AKSI, json_aksi);
                jsonBody.put(GlobalConfig.BAYAR_INVOICE_BAYAR, json_bayar);
                jsonBody.put(GlobalConfig.BAYAR_FK_TELEPON, s_nohp);
                jsonBody.put(GlobalConfig.BAYAR_FK_TOTAL, s_total);
                jsonBody.put(GlobalConfig.BAYAR_IS_BAYAR, ""+getIsBayar());
                jsonBody.put(GlobalConfig.BAYAR_ID_KET, getIDKet());
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());

                request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loading.dismiss();
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                        try {
                            int status      = response.getInt("status");
                            String message  = response.getString("message");
                            if(status==1){
                                tutupActivity(message);
                                if(alert_detail_piutang != null){
                                    myLog.print(getLocalClassName(), "dismiss");
                                    alert_detail_piutang.dismiss();
                                    alert_detail_piutang.cancel();
                                }
                            }else{
                                notifikasi(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //myLog.print("respons",response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                            switch(response.statusCode){
                                case 404:
                                    notifikasi("Terjadi masalah dengan server.");
                                    break;
                                case 408:
                                    notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                    break;
                                case 500:
                                    notifikasi("Terjadi masalah dengan server.");
                                    break;
                                default:
                                    notifikasi("Mohon maaf terjadi kesalahan.");
                                    break;
                            }
                        }
                    }
                }){
                    public Map<String, String> getHeaders() {
                        Map<String,String> headers = new Hashtable<String, String>();

                        //Adding parameters
                        headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }};

                request.setRetryPolicy(new DefaultRetryPolicy(
                        GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyAppController.getInstance().addToRequestQueue(request);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
        displayMessage(message);
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void onBackPressed()
    {
        tutupActivity("Pembayaran telah selesai.\nTerima kasih.");
    }
    private void tutupActivity(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        openDashboard();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    public void openDashboard(){
        Toko.this.finish();
    }

    private void bayarPiutang(int jumlah, String tgl, String faktur){
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_BAYAR_FAKTUR;
        //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);

        loading = ProgressDialog.show(Toko.this, "", "Mohon tunggu...", true);
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.ID_TOKO, ""));
//            jsonBody.put(GlobalConfig.UP_TGL_BAYAR, tgl);
//            jsonBody.put(GlobalConfig.UP_JUMLAH_BAYAR, jumlah);
//            jsonBody.put(GlobalConfig.UP_FAKTUR_BAYAR, faktur);
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            notifikasi_dialog("", message);
                            if(alert_detail_piutang != null){
                                myLog.print(getLocalClassName(), "dismiss");
                                alert_detail_piutang.dismiss();
                                alert_detail_piutang.cancel();
                            }
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private int getFromRadioGroup(RadioGroup rg){
        int radioButtonID = rg.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton)rg.findViewById(radioButtonID);
        int idx = (rg.indexOfChild(radioButton) +1);      //ditambah 1 karena id bermulain dari 0
        return idx;
    }
}